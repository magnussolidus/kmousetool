msgid ""
msgstr ""
"Project-Id-Version: kmousetool\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:40+0000\n"
"PO-Revision-Date: 2021-08-24 01:21+0100\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: none\n"
"X-POFile-SpellExtra: KMouseTool Olaf Jeff Schmi Gunnar Joe Roush Dt\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Schmidt Betts\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Pedro Morais,José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "morais@kde.org,zepires@gmail.com"

#: kmousetool.cpp:379
#, kde-format
msgid "The drag time must be less than or equal to the dwell time."
msgstr "O tempo de arrasto deve ser menor ou igual ao tempo de permanência."

#: kmousetool.cpp:379
#, kde-format
msgid "Invalid Value"
msgstr "Valor Inválido"

#: kmousetool.cpp:448 kmousetool.cpp:610
#, kde-format
msgid "&Stop"
msgstr "&Parar"

#: kmousetool.cpp:450 kmousetool.cpp:589 kmousetool.cpp:613
#, kde-format
msgctxt "Start tracking the mouse"
msgid "&Start"
msgstr "&Iniciar"

#: kmousetool.cpp:515
#, kde-format
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before closing the configuration window or "
"discard the changes?"
msgstr ""
"Existem alterações por gravar no módulo activo.\n"
"Deseja aplicar as alterações antes de fechar a janela de configuração ou "
"quer esquecer as modificações?"

#: kmousetool.cpp:517
#, kde-format
msgid "Closing Configuration Window"
msgstr "A Fechar a Janela de Configuração"

#: kmousetool.cpp:550
#, kde-format
msgid ""
"There are unsaved changes in the active module.\n"
"Do you want to apply the changes before quitting KMousetool or discard the "
"changes?"
msgstr ""
"Existem alterações por gravar no módulo activo.\n"
"Deseja aplicar as alterações antes de sair do KMouseTool ou quer esquecer as "
"modificações?"

#: kmousetool.cpp:551
#, kde-format
msgid "Quitting KMousetool"
msgstr "A Sair do KMouseTool"

#: kmousetool.cpp:592
#, kde-format
msgid "&Configure KMouseTool..."
msgstr "&Configurar o KMouseTool..."

#: kmousetool.cpp:595
#, kde-format
msgid "KMousetool &Handbook"
msgstr "&Manual do KMouseTool"

#: kmousetool.cpp:597
#, kde-format
msgid "&About KMouseTool"
msgstr "&Acerca do KMouseTool"

#. i18n: ectx: property (windowTitle), widget (QWidget, KMouseToolUI)
#: kmousetoolui.ui:14 main.cpp:28 main.cpp:30
#, kde-format
msgid "KMouseTool"
msgstr "KMouseTool"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: kmousetoolui.ui:20
#, kde-format
msgid "Settings"
msgstr "Configuração"

#. i18n: ectx: property (text), widget (QLabel, dragTimeLabel)
#: kmousetoolui.ui:62
#, kde-format
msgid "&Drag time (1/10 sec):"
msgstr "Tempo &de arrasto (1/10 s):"

#. i18n: ectx: property (text), widget (QLabel, movementLabel)
#: kmousetoolui.ui:97
#, kde-format
msgid "&Minimum movement:"
msgstr "&Movimento mínimo:"

#. i18n: ectx: property (text), widget (QCheckBox, cbStart)
#: kmousetoolui.ui:107
#, kde-format
msgid "Start with &desktop session"
msgstr "Iniciar com a sessão &do ambiente de trabalho"

#. i18n: ectx: property (text), widget (QCheckBox, cbDrag)
#: kmousetoolui.ui:114
#, kde-format
msgid "Smar&t drag"
msgstr "Arrastamento inteli&gente"

#. i18n: ectx: property (text), widget (QCheckBox, cbStroke)
#: kmousetoolui.ui:124
#, kde-format
msgid "&Enable strokes"
msgstr "Activar os &traços"

#. i18n: ectx: property (text), widget (QLabel, dwellTimeLabel)
#: kmousetoolui.ui:131
#, kde-format
msgid "Dwell &time (1/10 sec):"
msgstr "&Tempo de permanência (1/10 s):"

#. i18n: ectx: property (text), widget (QCheckBox, cbClick)
#: kmousetoolui.ui:154
#, kde-format
msgid "A&udible click"
msgstr "Click a&udível"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: kmousetoolui.ui:191
#, kde-format
msgid ""
"KMouseTool will run as a background application after you close this dialog. "
"To change the settings again, restart KMouseTool or use the system tray."
msgstr ""
"O KMouseTool vai passar a executar-se como uma aplicação de segundo plano "
"quando fechar esta janela. Para alterar a configuração, reinicie o "
"KMouseTool ou utilize a bandeja do sistema."

#. i18n: ectx: property (text), widget (QPushButton, buttonStartStop)
#: kmousetoolui.ui:223
#, kde-format
msgid "&Start"
msgstr "&Iniciar"

#: main.cpp:32
#, kde-format
msgid ""
"(c) 2002-2003, Jeff Roush\n"
"(c) 2003 Gunnar Schmidt "
msgstr ""
"(c) 2002-2003, Jeff Roush\n"
"(c) 2003 Gunnar Schmidt "

#: main.cpp:37
#, kde-format
msgid "Gunnar Schmidt"
msgstr "Gunnar Schmidt"

#: main.cpp:37
#, kde-format
msgid "Current maintainer"
msgstr "Manutenção actual"

#: main.cpp:38
#, kde-format
msgid "Olaf Schmidt"
msgstr "Olaf Schmidt"

#: main.cpp:38
#, kde-format
msgid "Usability improvements"
msgstr "Melhorias de usabilidade"

#: main.cpp:39
#, kde-format
msgid "Jeff Roush"
msgstr "Jeff Roush"

#: main.cpp:39
#, kde-format
msgid "Original author"
msgstr "Autor original"

#: main.cpp:41
#, kde-format
msgid "Joe Betts"
msgstr "Joe Betts"
